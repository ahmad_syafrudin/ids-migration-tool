﻿using Dapper;
using IDSMigrationTool.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDSMigrationTool
{
    public static class MT
    {
        private static string sqlQuery;

        private static async Task<SqlConnection> GetConnection(string conString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(conString);

                if (connection.State == ConnectionState.Open)
                    return connection;

                await connection.OpenAsync();

                return connection;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<SqlConnection> GetV1ConnectionAsync() 
            => await GetConnection(Properties.Settings.Default.IDSv1ConString);

        public static async Task<SqlConnection> GetV2ConnectionAsync()
            => await GetConnection(Properties.Settings.Default.IDSv2ConString);

        public static async Task<List<TenantV1>> GetTenantsAsync(string where = "", object param = null)
        {
            using (var con = await GetV1ConnectionAsync())
            {
                sqlQuery = $"SELECT * FROM Tenants {where} ORDER BY Code ASC";
                var tenants = await con.QueryAsync<TenantV1>(sqlQuery, param);

                return tenants.ToList();
            }

        }

        public static async Task<IEnumerable<Client>> GetClientsAsync(string where = "", object param = null)
        {
            using (var con = await GetV1ConnectionAsync())
            {
                sqlQuery = $"SELECT * FROM Clients {where} ORDER BY ClientId ASC";
                var clients = await con.QueryAsync<Client>(sqlQuery, param);

                return clients;
            }
        }
    }
}
