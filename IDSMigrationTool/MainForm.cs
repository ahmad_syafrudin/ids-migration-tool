﻿using Dapper;
using Dapper.Contrib.Extensions;
using IDSMigrationTool.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IDSMigrationTool
{
    public partial class MainForm : Form
    {
        private string sqlQuery;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitializeDefaultComponentValue();
        }

        private async void InitializeDefaultComponentValue()
        {
            TxtConStringV1.Text = Properties.Settings.Default.IDSv1ConString;
            TxtConStringV2.Text = Properties.Settings.Default.IDSv2ConString;

            await SetTenantDataSource();
            await SetClientDataSource();
        }

        private async Task SetTenantDataSource()
        {
            try
            {
                // clear current datasource
                CbTenant.DataSource = null;

                CbTenant.ValueMember = nameof(TenantV1.Id);
                CbTenant.DisplayMember = nameof(TenantV1.Code);

                CbTenant.DataSource = await MT.GetTenantsAsync();
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        private async Task SetClientDataSource()
        {
            try
            {
                CbClient.DataSource = null;

                CbClient.ValueMember = "ClientId";
                CbClient.DisplayMember = "ClientId";

                CbClient.DataSource = await MT.GetClientsAsync();
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        private async void TxtConStringV1_Validated(object sender, EventArgs e)
        {
            Properties.Settings.Default.IDSv1ConString = TxtConStringV1.Text;
            Properties.Settings.Default.Save();

            await SetTenantDataSource();
            await SetClientDataSource();
        }

        private void TxtConStringV2_Validated(object sender, EventArgs e)
        {
            Properties.Settings.Default.IDSv2ConString = TxtConStringV2.Text;
            Properties.Settings.Default.Save();
        }

        private async void BtnTestV1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = await MT.GetV1ConnectionAsync();

                MInfo("Connected!");
            }
            catch (Exception ex)
            {
                MWarning(ex.Message);
                Log(ex);
            }
        }

        private void Log(string message)
        {
            RbLogs.AppendText($"{DateTime.Now} {message}\n");

            RbLogs.Select(RbLogs.TextLength, 0);
            RbLogs.ScrollToCaret();
        }

        private void Log(Exception ex)
        {
            RbLogs.AppendText($"{DateTime.Now} {ex.Message.Replace(Environment.NewLine, "")}\n");

            if (ex.InnerException != null)
                RbLogs.AppendText($"{DateTime.Now} Inner Exception: {ex.InnerException.Message.Replace(Environment.NewLine, "")}\n");

            RbLogs.Select(RbLogs.TextLength, 0);
            RbLogs.ScrollToCaret();
        }

        private async void BtnTestV2_Click(object sender, EventArgs e)
        {
            try
            {
                var con = await MT.GetV2ConnectionAsync();

                MInfo("Connected!");
            }
            catch (Exception ex)
            {
                MWarning(ex.Message);
                Log(ex);
            }
        }

        private void MInfo(string message)
        {
            MessageBox.Show(message, "Info", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Information);
        }

        private void MWarning(string message)
        {
            MessageBox.Show(message, "Warning", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Warning);
        }

        private DialogResult MConfirm(string message)
        {
            return MessageBox.Show(message, "Info", buttons: MessageBoxButtons.OKCancel, icon: MessageBoxIcon.Question, defaultButton: MessageBoxDefaultButton.Button2);
        }

        private void BtnMigrate_Click(object sender, EventArgs e)
        {
            if (MConfirm($"This process cannot be undone. \r\nContinue to migrate data from IDS v1 to IDS v2 \r\nfor tenant '{CbTenant.Text.ToUpper()}'?") == DialogResult.OK)
            {
                MigrateNow();
            }
        }

        private async void MigrateNow()
        {
            Log("Migrating...");

            Log("Synchronize tenant");
            Progress(5, "Synchronize tenant");
            var tenantV2 = await SynchronizeTenantAsync();

            var isIds2 = await SchemaCheck();

            IEnumerable<AspNetUserV2> usersToSave = null;

            if (!isIds2)
            {
                Log("Get users from IDS v1");
                Progress(15, "Get users from IDS v1");
                var userV1s = await GetV1UsersAsync(CbTenant.SelectedValue.ToString());

                Log("Converting to IDS v2");
                Progress(35, "Converting to IDS v2");
                usersToSave = ConvertUsersV1ToV2(userV1s, tenantV2.Id);
            }
            else
            {
                usersToSave = await GetV2UsersToSaveAsync(CbTenant.SelectedValue.ToString(), tenantV2.Id);
            }

            Log("Save users to IDS v2");
            Progress(55, "Save users to IDS v2");
            int affectedRows = await SaveUsersV2(usersToSave, tenantV2.Name);

            Log($"Users created = {affectedRows:N0}");

            Log("Synchronize user role");
            Progress(75, "Synchronize user role");
            await SynchronizeUserRoleAsync(Guid.Parse(CbTenant.SelectedValue.ToString()), tenantV2.Id);

            Log("Synchronize clients");
            Progress(95, "Synchronize clients");
            await SynchronizeClients();

            Log("Synchronize api scope and resources");
            await SynchronizeApiScopesAndResources();

            ProgressDone();
            Log("Done!");
        }

        private async Task SynchronizeApiScopesAndResources()
        {
            try
            {
                using (var v1Con = await MT.GetV1ConnectionAsync())
                {
                    using (var v2Con = await MT.GetV2ConnectionAsync())
                    {
                        sqlQuery = "SELECT * FROM ApiResources";
                        var resources = await v1Con.QueryAsync<ApiResource>(sqlQuery);

                        foreach (var res in resources)
                        {
                            sqlQuery = "SELECT * FROM ApiResources WHERE Name = @Name";
                            var exist = await v2Con.QueryFirstOrDefaultAsync<ApiResource>(sqlQuery, new { res.Name });

                            if (exist == null)
                            {
                                sqlQuery = "INSERT INTO ApiResources (Enabled, Name, DisplayName, Description, Created, LastAccessed, NonEditable, Updated) VALUES (@Enabled, @Name, @DisplayName, @Description, @Created, @LastAccessed, @NonEditable, @Updated)";
                                await v2Con.ExecuteAsync(sqlQuery, res);
                            }
                        }


                        sqlQuery = "SELECT a.*, b.Name as ApiResourceName FROM ApiScopes a INNER JOIN ApiResources b ON a.ApiResourceId = b.Id ";
                        var scopes = await v1Con.QueryAsync<ApiScope>(sqlQuery);

                        scopes.ToList().ForEach(x => x.Created = DateTime.Now);

                        foreach (var sc in scopes)
                        {
                            sqlQuery = "SELECT * FROM ApiScopes WHERE Name = @Name";
                            var exist = await v2Con.QueryFirstOrDefaultAsync<ApiScope>(sqlQuery, new { sc.Name });

                            if (exist == null)
                            {
                                sqlQuery = "INSERT INTO ApiScopes (Name, DisplayName, Description, Required, Emphasize, ShowInDiscoveryDocument, ApiResourceId, Created, LastAccessed, NonEditable, Updated) VALUES (@Name, @DisplayName, @Description, @Required, @Emphasize, @ShowInDiscoveryDocument, (SELECT TOP 1 Id FROM ApiResources WHERE Name = @ApiResourceName), @Created, @LastAccessed, @NonEditable, @Updated)";
                                await v2Con.ExecuteAsync(sqlQuery, sc);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        private List<Client> GenerateDefaultClients()
        {
            var clients = new List<Client>
                        {
                            new Client
                            {
                                Enabled = true,
                                ClientId = "api.client",
                                ProtocolType = "oidc",
                                RequireClientSecret = true,
                                RequireConsent = true,
                                AllowRememberConsent = true,
                                AlwaysIncludeUserClaimsInIdToken = false,
                                RequirePkce = false,
                                AllowPlainTextPkce = false,
                                AllowAccessTokensViaBrowser = false,
                                FrontChannelLogoutSessionRequired = true,
                                BackChannelLogoutSessionRequired = true,
                                AllowOfflineAccess =  true,
                                IdentityTokenLifetime = 300,
                                AccessTokenLifetime = 3600,
                                AuthorizationCodeLifetime = 3600,
                                AbsoluteRefreshTokenLifetime = 2592000,
                                SlidingRefreshTokenLifetime = 1296000,
                                RefreshTokenUsage = 1,
                                UpdateAccessTokenClaimsOnRefresh = false,
                                RefreshTokenExpiration = 1,
                                AccessTokenType = 0,
                                EnableLocalLogin = true,
                                IncludeJwtId = false,
                                AlwaysSendClientClaims = false,
                                ClientClaimsPrefix = "client_",
                                Created = DateTime.Now,
                                DeviceCodeLifetime = 300,
                                NonEditable = false
                            },
                            new Client
                            {
                                Enabled = true,
                                ClientId = "web.client",
                                ProtocolType = "oidc",
                                RequireClientSecret = true,
                                RequireConsent = false,
                                AllowRememberConsent = true,
                                AlwaysIncludeUserClaimsInIdToken = true,
                                RequirePkce = true,
                                AllowPlainTextPkce = false,
                                AllowAccessTokensViaBrowser = true,
                                FrontChannelLogoutSessionRequired = true,
                                BackChannelLogoutSessionRequired = true,
                                AllowOfflineAccess =  true,
                                IdentityTokenLifetime = 300,
                                AccessTokenLifetime = 3600,
                                AuthorizationCodeLifetime = 3600,
                                AbsoluteRefreshTokenLifetime = 2592000,
                                SlidingRefreshTokenLifetime = 1296000,
                                RefreshTokenUsage = 1,
                                UpdateAccessTokenClaimsOnRefresh = false,
                                RefreshTokenExpiration = 1,
                                AccessTokenType = 0,
                                EnableLocalLogin = true,
                                IncludeJwtId = false,
                                AlwaysSendClientClaims = false,
                                ClientClaimsPrefix = "client_",
                                Created = DateTime.Now,
                                DeviceCodeLifetime = 300,
                                NonEditable = false
                            },
                            new Client
                            {
                                Enabled = true,
                                ClientId = "haermes",
                                ProtocolType = "oidc",
                                RequireClientSecret = true,
                                ClientName = "Haermes Client",
                                RequireConsent = false,
                                AllowRememberConsent = true,
                                AlwaysIncludeUserClaimsInIdToken = false,
                                RequirePkce = false,
                                AllowPlainTextPkce = false,
                                AllowAccessTokensViaBrowser = true,
                                FrontChannelLogoutUri = "https://localhost:44395/Account/LogOff",
                                FrontChannelLogoutSessionRequired = true,
                                BackChannelLogoutSessionRequired = true,
                                AllowOfflineAccess =  true,
                                IdentityTokenLifetime = 300,
                                AccessTokenLifetime = 3600,
                                AuthorizationCodeLifetime = 3600,
                                AbsoluteRefreshTokenLifetime = 2592000,
                                SlidingRefreshTokenLifetime = 1296000,
                                RefreshTokenUsage = 1,
                                UpdateAccessTokenClaimsOnRefresh = false,
                                RefreshTokenExpiration = 1,
                                AccessTokenType = 0,
                                EnableLocalLogin = true,
                                IncludeJwtId = false,
                                AlwaysSendClientClaims = false,
                                ClientClaimsPrefix = "client_",
                                Created = DateTime.Now,
                                DeviceCodeLifetime = 300,
                                NonEditable = false
                            }
                        };

            return clients;
        }

        private async Task SynchProcess(Client client, SqlConnection v1Con, SqlConnection v2Con)
        {
            if (client != null)
            {
                sqlQuery = "SELECT * FROM Clients WHERE ClientId = @ClientId";
                var v2Client = await v2Con.QueryAsync<Client>(sqlQuery, new { client.ClientId });

                if (v2Client == null || v2Client.Count() <= 0)
                {
                    await v2Con.InsertAsync(client);
                }
            }

            sqlQuery = $"SELECT a.* FROM ClientGrantTypes a INNER JOIN Clients b ON a.ClientId = b.Id WHERE b.ClientId = @ClientId";
            var grantTypes = await v1Con.QueryAsync<ClientGrantType>(sqlQuery, new { client.ClientId });
            if (grantTypes != null && grantTypes.Count() > 0)
            {
                var v2GrantTypes = await v2Con.QueryAsync<ClientGrantType>(sqlQuery, new { client.ClientId });

                if (v2GrantTypes == null || v2GrantTypes.Count() <= 0)
                {
                    foreach (var gt in grantTypes)
                    {
                        sqlQuery = "INSERT INTO ClientGrantTypes (GrantType, ClientId) VALUES (@GrantType, (SELECT Id FROM Clients WHERE ClientId = @ClientId));";
                        await v2Con.ExecuteAsync(sqlQuery, new { gt.GrantType, client.ClientId });
                    }
                }
            }

            sqlQuery = $"SELECT a.* FROM ClientPostLogoutRedirectUris a INNER JOIN Clients b ON a.ClientId = b.Id WHERE b.ClientId = @ClientId";
            var logoutRedirects = await v1Con.QueryAsync<ClientPostLogoutRedirectUri>(sqlQuery, new { client.ClientId });
            if (logoutRedirects != null && logoutRedirects.Count() > 0)
            {
                var v2LogoutRedirect = await v2Con.QueryAsync<ClientPostLogoutRedirectUri>(sqlQuery, new { client.ClientId });

                if (v2LogoutRedirect == null || v2LogoutRedirect.Count() <= 0)
                {
                    foreach (var lr in logoutRedirects)
                    {
                        sqlQuery = "INSERT INTO ClientPostLogoutRedirectUris (PostLogoutRedirectUri, ClientId) VALUES (@PostLogoutRedirectUri, (SELECT Id FROM Clients WHERE ClientId = @ClientId));";
                        await v2Con.ExecuteAsync(sqlQuery, new { lr.PostLogoutRedirectUri, client.ClientId });
                    }
                }
            }

            sqlQuery = $"SELECT a.* FROM ClientRedirectUris a INNER JOIN Clients b ON a.ClientId = b.Id WHERE b.ClientId = @ClientId";
            var redirectUris = await v1Con.QueryAsync<ClientRedirectUri>(sqlQuery, new { client.ClientId });
            if (redirectUris != null && redirectUris.Count() > 0)
            {
                var v2RedirectUri = await v2Con.QueryAsync<ClientRedirectUri>(sqlQuery, new { client.ClientId });

                if (v2RedirectUri != null || v2RedirectUri.Count() <= 0)
                {
                    foreach (var ru in redirectUris)
                    {
                        sqlQuery = "INSERT INTO ClientRedirectUris (RedirectUri, ClientId) VALUES (@RedirectUri, (SELECT Id FROM Clients WHERE ClientId = @ClientId));";
                        await v2Con.ExecuteAsync(sqlQuery, new { ru.RedirectUri, ClientId = client.ClientId });
                    }
                }
            }

            sqlQuery = $"SELECT a.* FROM ClientScopes a INNER JOIN Clients b ON a.ClientId = b.Id WHERE b.ClientId = @ClientId";
            var scopes = await v1Con.QueryAsync<ClientScope>(sqlQuery, new { client.ClientId });
            if (scopes != null && scopes.Count() > 0)
            {
                var v2Scope = await v2Con.QueryAsync<ClientScope>(sqlQuery, new { client.ClientId });

                if (v2Scope == null || v2Scope.Count() <= 0)
                {
                    foreach (var sc in scopes)
                    {
                        sqlQuery = "INSERT INTO ClientScopes (Scope, ClientId) VALUES (@Scope, (SELECT Id FROM Clients WHERE ClientId = @ClientId));";
                        await v2Con.ExecuteAsync(sqlQuery, new { sc.Scope, client.ClientId });
                    }
                }
            }

            sqlQuery = $"SELECT a.* FROM ClientSecrets a INNER JOIN Clients b ON a.ClientId = b.Id WHERE b.ClientId = @ClientId";
            var secrets = await v1Con.QueryAsync<ClientSecret>(sqlQuery, new { client.ClientId });
            if (scopes != null && scopes.Count() > 0)
            {
                var v2Secret = await v2Con.QueryAsync<ClientSecret>(sqlQuery, new { client.ClientId });

                if (v2Secret == null || v2Secret.Count() <= 0)
                {
                    foreach (var sc in secrets)
                    {
                        sqlQuery = "INSERT INTO ClientSecrets (Expiration, Description, Value, Type, ClientId, Created) VALUES (@Expiration, @Description, @Value, @Type, (SELECT Id FROM Clients WHERE ClientId = @ClientId), @Created);";
                        await v2Con.ExecuteAsync(sqlQuery, new { sc.Expiration, sc.Description, sc.Value, sc.Type, client.ClientId, sc.Created });
                    }
                }
            }
        }

        private async Task SynchronizeClients()
        {
            try
            {
                using (var v1Con = await MT.GetV1ConnectionAsync())
                {
                    using (var v2Con = await MT.GetV2ConnectionAsync())
                    {
                        string cbClient = CbClient.SelectedValue.ToString();

                        sqlQuery = "SELECT * FROM Clients WHERE ClientId = @ClientId";
                        var newClient = await v1Con.QueryFirstOrDefaultAsync<Client>(sqlQuery, new { ClientId = cbClient });

                        newClient.Created = DateTime.Now;
                        newClient.UserSsoLifetime = null;

                        var clients = GenerateDefaultClients();
                        clients.Add(newClient);

                        foreach (var client in clients)
                        {
                            await SynchProcess(client, v1Con, v2Con);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        private async Task SynchronizeUserRoleAsync(Guid tenantIdV1, Guid tenantIdV2)
        {
            try
            {
                using (var v1Con = await MT.GetV1ConnectionAsync())
                {
                    using (var v2Con = await MT.GetV2ConnectionAsync())
                    {
                        // get user role v1
                        sqlQuery = $"SELECT a.UserName, a.FirstName, a.TenantId, b.RoleId, c.Name as RoleName FROM AspNetUsers a INNER JOIN AspNetUserRoles b ON  a.Id = b.UserId AND a.TenantId = @TenantId INNER JOIN AspNetRoles c ON b.RoleId = c.Id";
                        var userRolesV1 = await v1Con.QueryAsync<UserRole>(sqlQuery, new { TenantId = tenantIdV1 });

                        // get roles v1
                        sqlQuery = "SELECT * FROM AspNetRoles ORDER BY Id";
                        var rolesV1 = await v1Con.QueryAsync<Role>(sqlQuery);

                        // key = v1 role name
                        // value = v2 role id
                        Dictionary<string, int> rolePairsV2 = new Dictionary<string, int>();

                        // synchronize roles in v2
                        foreach (var r in rolesV1)
                        {
                            sqlQuery = "SELECT * FROM AspNetRoles WHERE Name = @Name";
                            var roleV2 = await v2Con.QueryFirstOrDefaultAsync<Role>(sqlQuery, new { r.Name });

                            if (roleV2 == null)
                            {
                                // create new role in v2
                                roleV2 = new Role
                                {
                                    Name = r.Name,
                                    NormalizedName = r.NormalizedName
                                };

                                sqlQuery = "INSERT INTO AspNetRoles (Name, NormalizedName, ConcurrencyStamp) VALUES (@Name, @NormalizedName, @ConcurrencyStamp)";
                                await v2Con.ExecuteAsync(sqlQuery, new { roleV2.Name, r.NormalizedName, roleV2.ConcurrencyStamp });

                                sqlQuery = "SELECT * FROM AspNetRoles WHERE Name = @Name";
                                var tmp = await v2Con.QueryFirstOrDefaultAsync<Role>(sqlQuery, new { roleV2.Name });

                                roleV2.Id = tmp.Id;
                            }

                            rolePairsV2.Add(r.Name, roleV2.Id);
                        }

                        // get all users v2
                        sqlQuery = "SELECT * FROM AspNetUsers WHERE TenantId = @TenantId";
                        var usersV2s = await v2Con.QueryAsync<AspNetUserV2>(sqlQuery, new { TenantId = tenantIdV2 });

                        List<AspNetUserRole> dataToSave = new List<AspNetUserRole>();
                        foreach (var r in usersV2s)
                        {
                            var tmpUser = userRolesV1.FirstOrDefault(x => x.UserName == r.UserName && x.TenantId == tenantIdV1);

                            dataToSave.Add(new AspNetUserRole
                            {
                                UserId = r.Id,
                                RoleId = rolePairsV2[tmpUser?.RoleName]
                            });
                        }

                        // save user role v2 to database
                        sqlQuery = "INSERT INTO AspNetUserRoles (UserId, RoleId) VALUES (@UserId, @RoleId)";
                        int affectedRows = await v2Con.ExecuteAsync(sqlQuery, dataToSave);
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex);
            }
        }

        private async Task<IEnumerable<AspNetUserV1>> GetV1UsersAsync(string tenantId)
        {
            try
            {
                using (var v1Con = await MT.GetV1ConnectionAsync())
                {
                    sqlQuery = "SELECT * FROM AspNetUsers WHERE TenantId = @TenantId";
                    return await v1Con.QueryAsync<AspNetUserV1>(sqlQuery, new { TenantId = tenantId });
                }
            }
            catch (Exception ex)
            {
                Log(ex);
                return null;
            }
        }

        private async Task<IEnumerable<AspNetUserV2>> GetV2UsersToSaveAsync(string tenantId, Guid newTenantId)
        {
            try
            {
                using (var conn = await MT.GetV1ConnectionAsync())
                {
                    sqlQuery = "SELECT * FROM AspNetUsers WHERE TenantId = @TenantId";
                    var users = await conn.QueryAsync<AspNetUserV2>(sqlQuery, new { TenantId = tenantId });

                    users.ToList().ForEach(x => x.TenantId = newTenantId);
                    return users;
                }
            }
            catch (Exception ex)
            {
                Log(ex);
                return null;
            }
        }

        private IEnumerable<AspNetUserV2> ConvertUsersV1ToV2(IEnumerable<AspNetUserV1> users, Guid tenantIdV2)
        {
            try
            {
                return users.Select(x => new AspNetUserV2
                {
                    UserName = x.UserName,
                    NormalizedUserName = x.NormalizedUserName,
                    Email = x.Email,
                    NormalizedEmail = x.NormalizedEmail,
                    EmailConfirmed = x.EmailConfirmed,
                    PasswordHash = x.PasswordHash,
                    SecurityStamp = x.SecurityStamp,
                    ConcurrencyStamp = x.ConcurrencyStamp,
                    PhoneNumber = x.PhoneNumber,
                    PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                    TwoFactorEnabled = x.TwoFactorEnabled,
                    LockoutEnd = x.LockoutEnd,
                    LockoutEnabled = x.LockoutEnabled,
                    AccessFailedCount = x.AccessFailedCount,
                    TenantId = tenantIdV2,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    FullName = x.FirstName + " " + x.LastName,
                    CreateAt = DateTime.Now,
                    CreateBy = "IDSMigrationTool",
                    IsActive = x.IsActive
                });
            }
            catch (Exception ex)
            {
                Log(ex);
                return null;
            }

        }

        private async Task<int> SaveUsersV2(IEnumerable<AspNetUserV2> userV2s, string tenantName)
        {
            try
            {
                using (var v2Con = await MT.GetV2ConnectionAsync())
                {
                    foreach (var usr in userV2s)
                    {
                        sqlQuery = "INSERT INTO AspNetUsers (UserName, NormalizedUserName, Email, NormalizedEmail, EmailConfirmed, PasswordHash, SecurityStamp, ConcurrencyStamp, PhoneNumber, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnd, LockoutEnabled, AccessFailedCount, TenantId, FirstName, LastName, APIKey, IsActive, FullName, IsMasterAccount, IsAdministrator, UpdateAt, CreateAt, UpdateBy, CreateBy, NextPasswordChangeDate, IsLdap) VALUES (@UserName, @NormalizedUserName, @Email, @NormalizedEmail, @EmailConfirmed, @PasswordHash, @SecurityStamp, @ConcurrencyStamp, @PhoneNumber, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEnd, @LockoutEnabled, @AccessFailedCount, @TenantId, @FirstName, @LastName, @APIKey, @IsActive, @FullName, @IsMasterAccount, @IsAdministrator, @UpdateAt, @CreateAt, @UpdateBy, @CreateBy, @NextPasswordChangeDate, @IsLdap)";

                        await v2Con.ExecuteAsync(sqlQuery, usr);

                        sqlQuery = "SELECT * from AspNetUsers WHERE UserName=@UserName AND TenantId=@TenantId";
                        var newUser = await v2Con.QueryFirstOrDefaultAsync<AspNetUserV2>(sqlQuery, new { usr.UserName, usr.TenantId });

                        sqlQuery = $"INSERT INTO AspNetUserClaims (UserId,ClaimType,ClaimValue) VALUES " +
                            $"(@UserId,'name',@UserName), " +
                            $"(@UserId,'tenancy_name',@TenantName)";
                        var userClaim = await v2Con.ExecuteAsync(sqlQuery, new { UserId = newUser.Id, UserName = newUser.UserName, TenantName = tenantName });
                    }


                    return userV2s.Count();
                }
            }
            catch (Exception ex)
            {
                Log(ex);
                return -1;
            }

        }

        private async Task<TenantV2> SynchronizeTenantAsync()
        {
            try
            {
                using (var v1Con = await MT.GetV1ConnectionAsync())
                {
                    using (var v2Con = await MT.GetV2ConnectionAsync())
                    {
                        sqlQuery = "SELECT * FROM Tenants WHERE Id = @Id";
                        var selectedTenantV1 = await v1Con.QueryFirstOrDefaultAsync<TenantV1>(sqlQuery, new { Id = CbTenant.SelectedValue });

                        sqlQuery = "SELECT * FROM Tenants WHERE Code = @Code";
                        var tenantV2 = await v2Con.QueryFirstOrDefaultAsync<TenantV2>(sqlQuery, new { Code = selectedTenantV1.Code });

                        if (tenantV2 == null)
                        {
                            // copy tenant to IDS v2
                            tenantV2 = new TenantV2
                            {
                                Id = Guid.NewGuid(),
                                Code = selectedTenantV1.Code,
                                Name = selectedTenantV1.Name,
                                TenancyName = selectedTenantV1.TenancyName
                            };

                            sqlQuery = $"INSERT INTO Tenants (Id, Code, Name, TenancyName) VALUES (@Id, @Code, @Name, @TenancyName)";
                            await v2Con.ExecuteAsync(sqlQuery, tenantV2);
                        }

                        return tenantV2;
                    }
                }
            }
            catch (Exception ex)
            {
                Log(ex);

                return null;
            }
        }

        private void Progress(int value, string status)
        {
            PBar.Visible = true;
            PBar.Value = value;

            LblStatus.Visible = true;
            LblStatus.Text = status;
        }

        private void ProgressDone()
        {
            PBar.Visible = false;
            PBar.Value = 0;

            LblStatus.Visible = true;
            LblStatus.Text = "";
        }

        private void BtnClearLog_Click(object sender, EventArgs e)
        {
            RbLogs.Clear();
        }

        private void BtnSaveLog_Click(object sender, EventArgs e)
        {
            var saveDialog = new SaveFileDialog
            {
                DefaultExt = "txt",
                Filter = "Text Files|*.txt"
            };

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                RbLogs.SaveFile(saveDialog.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void BtnGenerateScript_Click(object sender, EventArgs e)
        {

        }

        private async Task<bool> SchemaCheck()
        {
            var v1Conn = await MT.GetV1ConnectionAsync();

            var tbl = await v1Conn.QueryAsync("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'AspNetUsers'");

            var isAdminstratorExist = tbl.Any(x => string.Equals(x.COLUMN_NAME, "IsAdministrator", StringComparison.OrdinalIgnoreCase));

            return isAdminstratorExist;
        }
    }
}
