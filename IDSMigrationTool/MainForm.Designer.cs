﻿
namespace IDSMigrationTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.PBar = new System.Windows.Forms.ToolStripProgressBar();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BtnClearLog = new System.Windows.Forms.Button();
            this.BtnSaveLog = new System.Windows.Forms.Button();
            this.RbLogs = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnGenerateScript = new System.Windows.Forms.Button();
            this.BtnMigrate = new System.Windows.Forms.Button();
            this.CbTenant = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnTestV2 = new System.Windows.Forms.Button();
            this.TxtConStringV2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnTestV1 = new System.Windows.Forms.Button();
            this.TxtConStringV1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CbClient = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PBar,
            this.LblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 599);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(500, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PBar
            // 
            this.PBar.Name = "PBar";
            this.PBar.Size = new System.Drawing.Size(100, 16);
            this.PBar.Visible = false;
            // 
            // LblStatus
            // 
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(12, 17);
            this.LblStatus.Text = "-";
            this.LblStatus.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BtnClearLog);
            this.groupBox3.Controls.Add(this.BtnSaveLog);
            this.groupBox3.Controls.Add(this.RbLogs);
            this.groupBox3.Location = new System.Drawing.Point(12, 297);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox3.Size = new System.Drawing.Size(476, 293);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Log";
            // 
            // BtnClearLog
            // 
            this.BtnClearLog.Location = new System.Drawing.Point(302, 257);
            this.BtnClearLog.Name = "BtnClearLog";
            this.BtnClearLog.Size = new System.Drawing.Size(75, 23);
            this.BtnClearLog.TabIndex = 4;
            this.BtnClearLog.Text = "Clear";
            this.BtnClearLog.UseVisualStyleBackColor = true;
            this.BtnClearLog.Click += new System.EventHandler(this.BtnClearLog_Click);
            // 
            // BtnSaveLog
            // 
            this.BtnSaveLog.Location = new System.Drawing.Point(383, 257);
            this.BtnSaveLog.Name = "BtnSaveLog";
            this.BtnSaveLog.Size = new System.Drawing.Size(75, 23);
            this.BtnSaveLog.TabIndex = 3;
            this.BtnSaveLog.Text = "Save";
            this.BtnSaveLog.UseVisualStyleBackColor = true;
            this.BtnSaveLog.Click += new System.EventHandler(this.BtnSaveLog_Click);
            // 
            // RbLogs
            // 
            this.RbLogs.BackColor = System.Drawing.SystemColors.ControlLight;
            this.RbLogs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RbLogs.Location = new System.Drawing.Point(13, 26);
            this.RbLogs.Name = "RbLogs";
            this.RbLogs.ReadOnly = true;
            this.RbLogs.Size = new System.Drawing.Size(445, 225);
            this.RbLogs.TabIndex = 2;
            this.RbLogs.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CbClient);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.BtnGenerateScript);
            this.groupBox2.Controls.Add(this.BtnMigrate);
            this.groupBox2.Controls.Add(this.CbTenant);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 184);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(476, 107);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Migration";
            // 
            // BtnGenerateScript
            // 
            this.BtnGenerateScript.Location = new System.Drawing.Point(246, 73);
            this.BtnGenerateScript.Name = "BtnGenerateScript";
            this.BtnGenerateScript.Size = new System.Drawing.Size(212, 23);
            this.BtnGenerateScript.TabIndex = 3;
            this.BtnGenerateScript.Text = "Generate  Script";
            this.BtnGenerateScript.UseVisualStyleBackColor = true;
            this.BtnGenerateScript.Visible = false;
            this.BtnGenerateScript.Click += new System.EventHandler(this.BtnGenerateScript_Click);
            // 
            // BtnMigrate
            // 
            this.BtnMigrate.Location = new System.Drawing.Point(13, 73);
            this.BtnMigrate.Name = "BtnMigrate";
            this.BtnMigrate.Size = new System.Drawing.Size(212, 23);
            this.BtnMigrate.TabIndex = 2;
            this.BtnMigrate.Text = "Migrate Data";
            this.BtnMigrate.UseVisualStyleBackColor = true;
            this.BtnMigrate.Click += new System.EventHandler(this.BtnMigrate_Click);
            // 
            // CbTenant
            // 
            this.CbTenant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbTenant.FormattingEnabled = true;
            this.CbTenant.Location = new System.Drawing.Point(13, 39);
            this.CbTenant.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.CbTenant.Name = "CbTenant";
            this.CbTenant.Size = new System.Drawing.Size(212, 21);
            this.CbTenant.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Select Tenant";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnTestV2);
            this.groupBox1.Controls.Add(this.TxtConStringV2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.BtnTestV1);
            this.groupBox1.Controls.Add(this.TxtConStringV1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(476, 166);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database Connection";
            // 
            // BtnTestV2
            // 
            this.BtnTestV2.Location = new System.Drawing.Point(406, 110);
            this.BtnTestV2.Name = "BtnTestV2";
            this.BtnTestV2.Size = new System.Drawing.Size(52, 23);
            this.BtnTestV2.TabIndex = 5;
            this.BtnTestV2.Text = "Test";
            this.BtnTestV2.UseVisualStyleBackColor = true;
            this.BtnTestV2.Click += new System.EventHandler(this.BtnTestV2_Click);
            // 
            // TxtConStringV2
            // 
            this.TxtConStringV2.Location = new System.Drawing.Point(13, 110);
            this.TxtConStringV2.Multiline = true;
            this.TxtConStringV2.Name = "TxtConStringV2";
            this.TxtConStringV2.Size = new System.Drawing.Size(387, 45);
            this.TxtConStringV2.TabIndex = 4;
            this.TxtConStringV2.Validated += new System.EventHandler(this.TxtConStringV2_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "IDS v2 Connection String (destination)";
            // 
            // BtnTestV1
            // 
            this.BtnTestV1.Location = new System.Drawing.Point(406, 39);
            this.BtnTestV1.Name = "BtnTestV1";
            this.BtnTestV1.Size = new System.Drawing.Size(52, 23);
            this.BtnTestV1.TabIndex = 2;
            this.BtnTestV1.Text = "Test";
            this.BtnTestV1.UseVisualStyleBackColor = true;
            this.BtnTestV1.Click += new System.EventHandler(this.BtnTestV1_Click);
            // 
            // TxtConStringV1
            // 
            this.TxtConStringV1.Location = new System.Drawing.Point(13, 39);
            this.TxtConStringV1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.TxtConStringV1.Multiline = true;
            this.TxtConStringV1.Name = "TxtConStringV1";
            this.TxtConStringV1.Size = new System.Drawing.Size(387, 45);
            this.TxtConStringV1.TabIndex = 1;
            this.TxtConStringV1.Validated += new System.EventHandler(this.TxtConStringV1_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IDS  Connection String (source)";
            // 
            // CbClient
            // 
            this.CbClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbClient.FormattingEnabled = true;
            this.CbClient.Location = new System.Drawing.Point(246, 39);
            this.CbClient.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.CbClient.Name = "CbClient";
            this.CbClient.Size = new System.Drawing.Size(212, 21);
            this.CbClient.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(246, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Select Client (IDS Config)";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 621);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar PBar;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BtnClearLog;
        private System.Windows.Forms.Button BtnSaveLog;
        private System.Windows.Forms.RichTextBox RbLogs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button BtnGenerateScript;
        private System.Windows.Forms.Button BtnMigrate;
        private System.Windows.Forms.ComboBox CbTenant;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnTestV2;
        private System.Windows.Forms.TextBox TxtConStringV2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnTestV1;
        private System.Windows.Forms.TextBox TxtConStringV1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CbClient;
        private System.Windows.Forms.Label label4;
    }
}