﻿using System;

namespace IDSMigrationTool.Models
{
    public class UserRole
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public Guid TenantId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
