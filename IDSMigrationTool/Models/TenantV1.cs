﻿using System;

namespace IDSMigrationTool.Models
{
    public class TenantV1
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TenancyName { get; set; }
    }
}
