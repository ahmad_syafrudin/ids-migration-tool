﻿using System;

namespace IDSMigrationTool.Models
{
    public class TenantV2
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TenancyName { get; set; }
        public bool RequireDigit { get; set; }
        public bool RequireLowercase { get; set; }
        public bool RequireUppercase { get; set; }
        public bool RequireNonAlphanumeric { get; set; }
        public int RequiredLength { get; set; }
        public int PasswordExpiryDays { get; set; }
        public bool EnableLdapLogin { get; set; }
        public string LdapPath { get; set; }
        public byte[] LdapManageUsername { get; set; }
        public byte[] LdapManagePassword { get; set; }
        public string LdapAdministratorMember { get; set; }
        public string LdapEmployeeMember { get; set; }
        public bool UpdatePasswordInUserSync { get; set; }
        public bool LogUserPassword { get; set; }
        public string LarkAppId { get; set; }
        public string LarkAppSecret { get; set; }
        public string LarkTenantKey { get; set; }
    }
}
