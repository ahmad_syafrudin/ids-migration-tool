﻿namespace IDSMigrationTool.Models
{
    public class AspNetUserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
