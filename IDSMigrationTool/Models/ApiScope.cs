﻿using System;

namespace IDSMigrationTool.Models
{
    public class ApiScope
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public bool Required { get; set; }
        public bool Emphasize { get; set; }
        public bool ShowInDiscoveryDocument { get; set; }
        public int ApiResourceId { get; set; }
        public DateTime Created { get; set; }
        public DateTime? LastAccessed { get; set; }
        public bool NonEditable { get; set; }
        public DateTime? Updated { get; set; }

        public string ApiResourceName { get; set; }
    }
}
