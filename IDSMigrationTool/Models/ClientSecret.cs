﻿using System;

namespace IDSMigrationTool.Models
{
    public class ClientSecret
    {
        public DateTime? Expiration { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public int ClientId { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
    }
}
